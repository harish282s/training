package com.securityapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Second
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		// TODO Auto-generated method stub
		/***** Set Response Content Type *****/
		response.setContentType("text/html");
		/***** Print The Response *****/
		PrintWriter out = response.getWriter();

		try {

//		String title = "Public Page";
//		String docType = "<!DOCTYPE html>\n";
//		out.println(docType + "<html>\n"
//				+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><title>" + title
//				+ "</title></head>\n" + "<body>");
//
//		out.println("<h2>Admin Servlet</h2>"
//				+ "<div> Servlet Basic Demo: <span>Admin Servlet</span></div>"
//				+ "</body>\n</html>");
//		
//		RequestDispatcher rd = request.getRequestDispatcher("Admin.jsp");
//
//        rd.forward(request,response);
			response.sendRedirect(request.getContextPath() + "/Admin.jsp");

			// System.out.println("id: " +session.getId());

		} finally {
			out.close();
		}

	}
}
