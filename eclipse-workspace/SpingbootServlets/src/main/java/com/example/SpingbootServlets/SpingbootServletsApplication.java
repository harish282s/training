package com.example.SpingbootServlets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpingbootServletsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpingbootServletsApplication.class, args);
	}

}
