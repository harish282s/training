package com.example.SpingbootServlets;



import java.io.IOException;
import java.io.PrintWriter;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Second
 */
@WebServlet("/UserServlet")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/***** Set Response Content Type *****/
		response.setContentType("text/html");
		/***** Print The Response *****/
		PrintWriter out = response.getWriter();
		
//		String title = "Public Page";
//		String docType = "<!DOCTYPE html>\n";
//		out.println(docType + "<html>\n"
//				+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><title>" + title
//				+ "</title></head>\n" + "<body>");
//
//		out.println("<h2>User Servlet</h2>"
//				+ "<div> Servlet Basic: <span>User Servlet</span></div>"
//				+ "</body>\n</html>");
//		
//
//		RequestDispatcher rd = request.getRequestDispatcher("User.jsp");
//
//        rd.forward(request,response);
		
		response.sendRedirect(request.getContextPath() + "/User.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}

}
