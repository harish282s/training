package com.example.Trackerapp.models;

public class Location {
	
	private String state;
	private String country;
	private int latestTotalcases;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getLatestTotalcases() {
		return latestTotalcases;
	}
	public void setLatestTotalcases(int latestTotalcases) {
		this.latestTotalcases = latestTotalcases;
	}
	@Override
	public String toString() {
		return "Location [state=" + state + ", country=" + country + ", latestTotalcases=" + latestTotalcases + "]";
	}
	

}
