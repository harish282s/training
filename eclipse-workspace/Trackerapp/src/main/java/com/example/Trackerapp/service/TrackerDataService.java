package com.example.Trackerapp.service;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.Trackerapp.models.Location;

@Service
public class TrackerDataService {
	
	private static String VIRUS_DATA_URL="https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";
 
	private List<Location> stats =new ArrayList<>();

	public List<Location> getStats() {
		return stats;
	}

	@PostConstruct
	@Scheduled(cron ="* * * 1 * *")
	public void fetchData() throws IOException, InterruptedException {
		
		List<Location> newstats =new ArrayList<>();
		
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(VIRUS_DATA_URL))
				.build();
	
			HttpResponse<String> response=client.send(request, HttpResponse.BodyHandlers.ofString());
			//System.out.println(response.body());
			
			StringReader reader = new StringReader(response.body());
			
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);
			for (CSVRecord record : records) {
				Location location =new Location();
				location.setState(record.get("Province/State"));
				location.setCountry(record.get("Country/Region"));
				//location.setLatestTotalcases(Integer.parseInt(record.get("latestTotalcases")));
				location.setLatestTotalcases(Integer.parseInt(record.get(record.size()-1)));
				//System.out.println(location);
				
				newstats.add(location);
				
			    //String state = record.get("Province/State");
			    //System.out.println(state);
			    
			    //String customerNo = record.get("CustomerNo");
			    //String name = record.get("Name");
			}
			 this.stats=newstats;
	}
	
}
