package com.example.Trackerapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.Location;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Trackerapp.service.TrackerDataService;

@RestController
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	TrackerDataService trackerdataservice;
	 
	@RequestMapping("/")
	public String home(Model model)
	{
		List<com.example.Trackerapp.models.Location> allstats=trackerdataservice.getStats();
		int totalReportedCases =allstats.stream().mapToInt(stat->stat.getLatestTotalcases()).sum();
		model.addAttribute("locationstats",allstats);
		model.addAttribute("totalReportedCases",totalReportedCases);
	    //model.addAttribute("locationstats",trackerdataservice.getStats());
		return "home";
	}

}
